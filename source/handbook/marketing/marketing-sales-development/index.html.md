---
layout: markdown_page
title: "Marketing & Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is Marketing & Sales Development Handbook

The Marketing & Sales Development organization includes the Sales Development, Content Marketing, Field Marketing, Online Marketing, and Marketing Operations teams. The teams in this organization employ a variety of marketing and sales crafts in service of our current and future customers with the belief that providing our audiences value will in turn grow GitLab's business.

## Marketing & Sales Development Handbooks  <a name="handbooks"></a>

- [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
- [Content Marketing](/handbook/marketing/marketing-sales-development/content)
- [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
- [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)

## Demand Waterfall <a name="waterfall"></a>

A demand waterfall is a term used to track the different milestones prospective customers go through as they learn about GitLab and interact with our sales and marketing teams. Each subsequent milestone is a subset of the previous milestone, and represents a progression from not knowing GitLab to being a customer (and fan) of GitLab.

At the highest level of abstraction, we use the terms `lead`, `opportunity`, and `customer` to represent a person's progression towards becoming a customer of GitLab. Those three terms also correspond to record types in salesforce.com.

Lead => Opportunity => Customer

However, there are more granular steps within the above milestones that are used to track the above process with more precision. They are tracked as follows:

| Funnel stage | Record Type | Status or Stage |
|---------------|--|----------------------|-------------------|
| Raw | [Lead or Contact] | Raw |
| Inquiry | [Lead or Contact] | Inquiry |
| Marketo Qualified Lead | [Lead or Contact] |  MQL |
| Accepted Lead | [Lead or Contact] |  Accepted |
| Qualifying | [Lead or Contact] |  Qualifying |
| Qualified Lead | [Opportunity] | 0 - Pre AE Qualified |
| [Sales Accepted Opportunity] | [Opportunity] | 1 - Discovery |
| Sales Qualified Opportunity | [Opportunity] | 2 Scoping - 5 Negotiating |
| Customer | [Opportunity] | 6-Closed Won |

For the definition of the stage please click the record type link.

When going from Qualifying to Qualified Lead the lead is duplicated to an opportunity, and the lead is set to qualified and not being used anymore.

## References<a name="references"></a>

- [Marketing & Sales Development Roles, Process, and 2H 2017 Plan](https://docs.google.com/presentation/d/1NBsqRudh5ELNFHRJCjt5yxLNRrigv16qC0-_sWWAcpU/edit#slide=id.g23e881c565_0_136)
- [Reseller Handbook](/handbook/resellers/)

[Lead or Contact]: /handbook/marketing/marketing-sales-development/sdr/inbound/#statuses
[Opportunity]: /handbook/sales/#opportunity-stages
[Sales Accepted Opportunity]: /handbook/marketing/marketing-sales-development/sdr/outbound/#acceptedopp
