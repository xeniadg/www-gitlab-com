---
layout: markdown_page
title: "Sales Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

As a BDR (Business Development Representative) or SDR (Sales Development Representative) you will be dealing with the front end of the sales process. Your focus will be on generating opportunities that the AEs (Account Executives) and SALs (Strategic Account Leaders) accept, ultimately leading to closed won business. On this team we work hard, but have fun too (I know, it's a cliche ...but here it's true!). We will work hard to guarantee your success if you do the same. We value results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, solutions, and quirkiness.

Being a BDR or SDR can come with what seems like long days, hard work, frustration, and even at times, failure. If you have the right mindset and come to work with a tenacious, hungry attitude you will see growth personally and in your career. GitLab is a place with endless opportunity. Let’s make it happen!

### [Inbound](/handbook/marketing/marketing-sales-development/sdr/inbound)
### [Outbound](/handbook/marketing/marketing-sales-development/sdr/outbound)

## Criteria for Sales Accepted Opportunity (SAO)<a name="acceptedopp"></a>

* Account is a Mid Market, Large or Strategic account (SDRs may only get credit for SAOs in Large or Strategic accounts)
* Right person/right profile - can influence a purchase of GitLab as an evaluator, decision maker, technical buyer, or influencer.
* High level business need that GitLab can address
* Prospects wants to learn more about how GitLab can address their stated business need
* The prospect has committed to a next call to go deeper into their current situation and needs
* An opportunity should immediately be moved into stage 1 - Discovery (by AE/SAL) if it meets the above criteria
* If user count is determined after the initial discovery call, the Account Executive should update the Opportunity name accordingly


## Training and Resources

You play a crucial role that helps bridge the gap between sales and marketing. As you gain knowledge, you will be able to aid our future customers ship better software, faster. There are numerous resources at your fingertips that we have created to help you in this process. 

### Process 
- [Revenue OPS Handbook](/handbook/revenue-ops/)
- [Marketing Handbook](/handbook/marketing/)
- [Sales Handbook](/handbook/sales/)
- [Customer Success Handbook](https://about.gitlab.com/handbook/customer-success/)
- [Resellers Handbook](https://about.gitlab.com/handbook/resellers/)
- [Support Handbook](https://about.gitlab.com/handbook/support/)

### Product
- [GitLab Resources](https://about.gitlab.com/resources/)
- [GitLab Blog](https://about.gitlab.com/blog/)
- [GLU GitLab University](https://docs.gitlab.com/ce/university/) 
- [GitLab Primer](https://about.gitlab.com/primer/)
- [Glossary of Terms](https://docs.gitlab.com/ee/university/glossary/README.html#what-is-the-glossary)
- [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
- [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
- [Sales Qualification Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
- [GitLab Sales Demo](https://about.gitlab.com/handbook/sales/demo/)
- [FAQ from Prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
- [Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
- [Platzi GitLab Workshop](https://courses.platzi.com/classes/git-gitlab/)
- [GitLab Market and Ecosystem](https://www.youtube.com/watch?v=sXlhgPK1NTY&list=PLFGfElNsQthbQu_IWlNOxul0TbS_2JH-e&index=6)
- [GitLab Documentation](http://docs.gitlab.com/ee/)
- [GitLab Compared to Other Tools](https://about.gitlab.com/comparison/)
- [GitLab Battlecards](https://docs.google.com/document/d/1zRIvk4CaF3FtfLfSK2iNWsG-znlh64GNeeMwrTmia_g/edit#)
- [Version.gitlab](https://version.gitlab.com/users/sign_in)
- [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
- [Additional Training Resources](https://drive.google.com/drive/folders/0B1W9hTDXggO1NGJwMS12R09lakE)

### Tools
- [Tech Stack](https://about.gitlab.com/handbook/revenue-ops/#tech-stack)
- [Tools and Tips](https://about.gitlab.com/handbook/tools-and-tips/)
- [Getting Started with Slack](https://get.slack.help/hc/en-us/articles/218080037-Getting-started-for-new-members)
- [Grovo](https://www.grovo.com/)
- [Outreach University](https://university.outreach.io/)
- [How to Use Outreach](https://docs.google.com/document/d/1FzvGEsL6ukxFZtk7ZkMUVAAT_wW-aVblAu1yOFWiEGo/edit)
- [Drift Agent Training Guide](https://help.drift.com/getting-started/drift-agent-training-guide)

### Personal Development


## Asking questions

Don't hesitate to ping one of your colleagues with a question, or someone on the team who specializes in what you're searching for. Everyone is happy to help, and it's better to get your work done faster with your team, than being held up at a road block.
- [Who to Talk to for What](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)

## Improvements/Contributing

- If you get an answer to a question someone else may benefit from, incorporate it into this handbook or add it to the FAQ document
- After meetings or process changes, feel free to update this handbook, and submit a merge request.
- Create issues for any idea (small or large that), that you want feedback on
- All issued and MRs for changes to the SDR handbook assign to Chet Backman
