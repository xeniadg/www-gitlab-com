---
layout: markdown_page
title: "Inbound BDR"
---
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Inbound BDR Handbook<a name="inbound"></a>

{:toc}

### Onboarding<a name="onboard"></a>
You will be assigned an onboarding issue by Peopleops. Tasks in the issue will fill up the majority of your first week. This is a step by step guide/checklist to getting everything in your arsenal set up, such as equipment, tools, security, and your Gitlab.com account. These todo’s provide you with the fundamentals.

In addition to your onboarding issue, you will follow the BDR Onboarding Schedule here.

### BDR Standards

#### Other measurements<a name="measurements"></a>

BDRs will also be measured on the following:

* Results
  * Pipeline value of BDR generated opportunities
  * IACV won from opportunities BDR generated
* Activity
  * Number of opportunities created
  * Number of emails sent
  * Number of calls made

Note, while important, the above measurements do not impact your quota attainment. Your main focus will be achieving your SAO quota.

### BDR Process

#### BDR & BDR Team Lead<a name="teamLead"></a>
##### Formal Weekly 1:1
- Mental check-in (winning and success)
- Coaching - email strategy, tools, campaigns, cadence, best practices
- Review goals at the account level and personal level
- Strategy for next week
- Upcoming events/campaigns that can be leveraged
- Personal goals and commitments

### Inbound Workflow<a name="bdring"></a>
- Strategize to develop the proper qualifying questions for all types of prospective customers.
- Be able to identify where a prospective customer is in their buying cycle and take appropriate action to help them along their journey towards becoming a customer.
- Generate Sales Accepted Opportunities (SAOs)
- Inbound to work off of leads within SFDC
- Inbound does not touch any lead that has activity on it within the last 45 days by a different BDR.

### Researching<a name="researching"></a>
#### 3 X 3 X 3
Three things in three minutes about the:
- Organization
- Industry
- Prospect/Lead

### Salesforce
- Search Salesforce.com (SFDC) for the company name.
    - Are there any duplicate leads or contacts?
    - Are there other leads from the organization that are being worked?
    - Activity History
        - Who in the organization have we been in contact with? Can we follow up or reference this?
    - Is there an owned customer/prospect account?
    - Are there any open opportunities?

### LinkedIn
- Summary/bio
    - Does anything stand out that is relevant to their needs as an organization?
    - What is the lead’s role and how does that affect your messaging?
    - Have they published any articles that would be worth referencing?
- Previous work
    - Any Gitlab Customers that we can reference?
- Connections
    - Are they connected to anyone at GitLab for a possible introduction?

### Company Website
- Mission Statement
- Press Releases/Newsroom
- (ctrl F) search for keywords

### Prospecting<a name="prospecting"></a>

### Email Prospecting<a name="emailProspect"></a>
- Studies show that Executives read emails in the morning
- Expect to be forwarded to the right person (Always ask)
- Keep emails 90 words or less
- Break into readable stanzas
- Always be specific and tailored
- Make your emails viewable from a mobile device

#### Structuring a Prospecting Email
- Subject line
- Preview pain
- Opening stanza
    - Make it about them, not about you
- Benefit and value proposition
- Call to action (CTA)
    - Offer available times for a meeting/call
    - Create urgency

### Call Prospecting<a name="callProspect"></a>
- Call about them, not about you
- Be confident and passionate
- Aim for every role but focus on technical decision makers
- Ask for time
- Focus on your endgame: Sales Accepted Opportunities (SAO)
- Make it easy to say yes
- Obtain a commitment

### Structuring a Prospecting Call
- Introduction
    - Immediately introduce yourself and GitLab
    - Ask for time
- Initial benefit statement
- Qualification
- Inform them/answer questions about GitLab
- Commitments

### Warm Discovery Calls
Purpose: To qualify leads faster and more accurately (Sales Acceptance Criteria) than in an email exchange.

Process: In your reply message to setup/initiate a call, ask a few of your normal BDR questions to prep for the call. To save a step in emails include your Calendly link so leads may schedule the call directly.

    “Hi (lead name), this is (BDR) from GitLab. How are you today?”

    “Great, is now still a good time to talk about (primary issue)?”

After you’ve established the conversation is good to move forward, ask questions and guide the conversation in a way that enables the lead to tell you what their issue/problem is while also providing answers to the sales acceptance criteria. Your primary role is to gather information, and decide more appropriately how to provide assistance and/or qualify the lead for a call with an AE.

    “Great (lead name), thank you for sharing that information. I have a better feel now for how to move forward with your request/issue. I’m going to follow-up with an email recapping what we discussed, send over that documentation I promised and get you in touch with (account executive)”

### Qualifying<a name="qualifying"></a>
Your goal is to generate Sales Accepted Opportunities (SAOs) by gathering all pertinent information needed for an Account Executive to take action. Some examples of sales qualification questions can be found [here](https://about.gitlab.com/handbook/sales-qualification-questions/).

### Miscellaneous<a name="miscellaneous"></a>

### Variable Compensation Guidelines<a name="compensation"></a>
Full-time Inbound BDRs have a significant portion of their pay based on performance and objective attainment. Performance based "bonuses" are based on quota attainment.

Actions for obtaining results will be prescribed and measured, but are intentionally left out of bonus attainment calculations to encourage experimentation. Inbound BDRs will naturally be drawn to activities that have the highest yield, but freedom to choose their own daily activities will allow new, higher yield activities to be discovered.

### Guidelines for Bonuses
- Team and individual quotas are based on GitLab's revenue targets
- Quotas will be made known by having each BDR sign a participant form that clearly lays out quarterly quotas that match the company's revenue plan
- Bonuses are paid quarterly.
- Bonuses are based solely on sales accepted opportunities generated. Guidelines for a [sales accepted opportunity](/handbook/marketing/marketing-sales-development/sdr/#acceptedopp)
- A new BDR's first month's bonus is typically based on completing onboarding
