---
layout: markdown_page
title: "Revenue Operations"
---
<a name="topofpage"></a>
## On this page
{:.no_toc}

- TOC
{:toc}  


## In this section
- [Customer Lifecycle](/handbook/revenue-ops/customer-lifecycle)
- [Database Management](/handbook/revenue-ops/database-management)
- [Reporting](/handbook/revenue-ops/reporting)



## Purpose   
Since parts of all three functional groups work closely together and operational management of the database needs to seamlessly move from one functional group to another, this section is a singular reference point for all three teams to find the most up-to-date workflows, routing, rules and definition sets. Overview of workflow and operational processes that overlap the [Sales](https://about.gitlab.com/handbook/sales), [Marketing](https://about.gitlab.com/handbook/marketing) and [Customer Success](https://about.gitlab.com/handbook/customer-success/) teams. A singular repository to track all tools, license allocation, admins and contracts for the combined Marketing & Sales Operations tech stack. Tips, tricks and how to's for the various applications and external resources that will help user be more productive in the business systems.    


## Reaching the Teams (internally)   
- **Public Issue Tracker**: A short list of the main, public issue trackers. For respective teams, there may be additional resources. Please use confidential issues for topics that should only be visible to team members at GitLab 
    - [Sales](https://gitlab.com/gitlab-com/sales/issues/) - general sales related needs & issues
    - [Salesforce](https://gitlab.com/gitlab-com/salesforce/issues) - Salesforce specific needs, issues and questions
    - [Marketing](https://gitlab.com/gitlab-com/marketing/issues) - All issues related to website, product, design, events, webcasts, lead routing, social media and community relations
    - [Customer Success SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues) - your go to place for pre-sales, post-sales, customer training and professional services
- **Email**: You can send an email to the respective team you wish to reach, please reference the ['GitLab Email Forwarding' Google doc](https://docs.google.com/document/d/1rrVgD2QVixuAf07roYws7Z8rUF2gJKrrmTE4Z3vtENo/edit#) for the appropriate alias.  
- **Slack**: A short list of the primary Slack channels used by these respective teams  
    - `#sales`
    - `#marketing`
    - `#solutions_architect`
    - `#bdr-team`
    - `#sdr`
    - `#sfdc-users`
    - `#lead-questions`
 

## Tech Stack   
| Applications & Tools | Who Should Have Access | Whom to Contact w/Questions | Admin(s) |
| :--- | :--- | :--- | :--- |
| **Clearbit** | Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR | **JJ Cordz (primary)**<br>Francis Aquino | JJ Cordz<br>Francis Aquino |
| **DiscoverOrg** | Mid-Market AEs<br>Outbound SDR | **Francis Aquino** | Francis Aquino |
| **DoGood** | Online Marketing | **Mitchell Wright** | Mitchell Wright |
| **Drift** | Inbound BDR | **Mitchell Wright (primary)**<br>JJ Cordz (integration related) | Mitchell Wright<br>JJ Cordz |
| **FunnelCake** | Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing |  | JJ Cordz |
| **Google Analytics** | Sr Director Demand Gen<br>Marketing OPS<br>Online Marketing<br>Product Marketing<br>Content Marketing |  | Mitchell Wright<br>JJ Cordz |
| **Google Tag Manager** | Online Marketing |  | Mitchell Wright<br>JJ Cordz |
| **GovWin IQ** | Sales - Federal | **Francis Aquino** | Francis Aquino |
| **InsightSquared** | Executives<br>Sales Leadership | **Francis Aquino** | Francis Aquino |
| **LeanData** | Marketing OPS<br>Sales OPS | **JJ Cordz (primary)**<br>Francis Aquino | JJ Cordz<br>Francis Aquino |
| **LicenseApp** | Access via GitLab credentials | **Francis Aquino** |  |
| **LinkedIn Sales Navigator** | Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR | **JJ Cordz** | JJ Cordz<br>Francis Aquino |
| **Marketo** | Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing | **JJ Cordz (primary)**<br>Mitchell Wright | JJ Cordz<br>Mitchell Wright |
| **Optimizely** | Online Marketing |  | Mitchell Wright |
| **Outreach.io** | Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR | **Francis Aquino (primary)**<br>JJ Cordz<br>Chet Backman | Francis Aquino<br>JJ Cordz<br>Chet Backman |
| **Piwik** | Online Marketing |  | Michell Wright |
| **Salesforce** | Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR<br>Solutions Architects<br>Marketing OPS | *Not related to quotes or lead routing*: **Francis Aquino**<br><br>*Lead Routing/Scoring/Source*: **JJ Cordz** | Francis Aquino<br>JJ Cordz |
| **Sertifi** | Account Executives<br>Account Managers | **Francis Aquino** | Francis Aquino |
| **Terminus** | Online Marketing |  | Mitchell Wright |
| **Unbounce** | Marketing OPS<br>Online Marketing<br>Content Marketing<br>Field Marketing<br>Design | **Mitchell Wright**<br>JJ Cordz | Mitchell Wright<br>JJ Cordz |
| **WebEx** | Marketing OPS | **JJ Cordz** | JJ Cordz |
| **Zendesk** | Access via Salesforce | **Lee Matos** | Francis Aquino |
| **Zoom** | Access via GitLab credentials | **PeopleOPS** | PeopleOPS |
| **Zuora** | Access via Salesforce | **Wilson Lau (primary)**<br>Francis Aquino (secondary) | Francis Aquino |

[Operations License Tracking & Contract Details](https://docs.google.com/a/gitlab.com/spreadsheets/d/1-W0rmZalDOpTNQf4K50DZ1y3slgmNSrPVPuF6_Hfz6s/edit?usp=sharing) (document can only be accessed by GitLab team members)



## Glossary  <a name="glossary"></a>   

| Term | Definition |
| :--- | :--- |
| Accepted Lead | A lead a Sales Development Representative or Business Development Represenative agrees to work until qualified in or qualified out |
| Account | An organization tracked in salesforce.com. An account can be a prospect, customer, former customer, integrator, reseller, or prospective reseller |
| AM | Account Manager |
| AE | Account Executive |
| APAC | Asia-Pacific |
| BDR | Business Development Representative |
| CS | Customer Success |
| EMEA | Europe, Middle East and Asia |
| Inquiry | an Inbound request or response to an outbound marketing effort |
| IQM | Initial Qualifying Meeting |
| MQL | Marketo Qualified Lead - an inquiry that has been qualified through systematic means (e.g. through demographic/firmographic/behaviour lead scoring) |
| NCSA | North, Central, South America |
| Qualified Lead | A lead a Sales Development Representative or Business Development Representative has qualified, converted to an opportunity and assigned to a Sales Representative (Stage `0-Pending Accpetance`) |
| RD | Regional Director |
| Sales Admin | Sales Administrator |
| [SAO] | Sales Accepted Opportunity - an opportunity Sales agrees to pursue following an Inital Qualifying Meeting |
| SDR | Sales Development Representative |
| SLA | Service Level Agreement |
| SCLAU | Abbreviation for SAO (Sales Accepted Opportunity) Count Large and Up |
| SQO | Sales Qualified Opportunity |
| TEDD | Technology, Engineering, Development and Design - used to estimate the maximum potential users of GitLab at a company |
| Won Opportunity | Contract signed to Purchase GitLab |      


## Other Related Pages

- [SDR Handbook](/handbook/marketing/marketing-sales-development/sdr/)
- [Sales Operations](/handbook/sales/sales_ops/)
- [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)   
- [Customer Success](/handbook/customer-success/)





[Return to Top of Page](#topofpage)

[SAO]: /handbook/marketing/marketing-sales-development/sdr/#criteria-for-sales-accepted-opportunity-sao