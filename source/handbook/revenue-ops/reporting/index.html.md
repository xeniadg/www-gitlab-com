---
layout: markdown_page
title: "Revenue OPS - Reporting"
---

## On this page
{:.no_toc}

- TOC
{:toc}




## Reporting

The Online Marketing team is in charge of updating reports and dashboards for the weekly marketing meetings. The current reporting that is being used can be found in [Google Drive](https://drive.google.com/open?id=0B1_ZzeTfG3XYMWc4U0RiRVcxcDA).

To access numbers for the reports, you can use the following links:
- [Web Traffic](https://analytics.google.com/analytics/web/#report/acquisition-channels/a37019925w65271535p67064032/)
- [Unique Visitors](https://analytics.google.com/analytics/web/#report/visitors-overview/a37019925w65271535p67064032/)
- [Respondents (leads)](https://app-ab13.marketo.com/#AR1358A1)
- [MQLs](https://app-ab13.marketo.com/#AR1365A1) (This is a little complicated as this report actually gives you lead creation date, so you need to go to `Smart List` -> `View Qualified Leads` then export and use a pivot table in Excel to get the actual numbers. I'm open to ideas if there's a better way to get the MQL numbers)
- [SQL #](https://na34.salesforce.com/00O61000003no6p)
- [SQL $ Value](https://na34.salesforce.com/00O61000003no6p)
- [Won #](https://na34.salesforce.com/00O61000003no6z)
- [Won $ Amount](https://na34.salesforce.com/00O61000003no6z)