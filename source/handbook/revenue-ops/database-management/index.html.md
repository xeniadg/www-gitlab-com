---
layout: markdown_page
title: "Revenue Operations - Database Management"
---
<a name="topofpage"></a>
## On this page
{:.no_toc}

- TOC
{:toc}  


## Types of Accounts


### Accounts Created in Salesforce utilizing CE Usage Ping Data   
The [CE Usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) provides GitLab with some limited insight into how end users are utilizing the platform. The raw information is cleaned, enriched and then pushed to SFDC as an Account by the BizOPS team.  

If there is not an existing account match in Salesforce, a new account record will be created with the following information populated:    

| SFDC Field | Default Value |
|---|---|
| Account Name |  |
| Number of Employees |  |
| Billing Street |  |
| Billing City |  |
| Billing Zip |  |
| Billing Country |  |
| Account Type | `Prospect - CE User` |
| Account Website |  |
| Industry | Populated by Clearbit |
| Active CE Users | Populated by Usage Ping |
| CE Instances | Populated by Usage Ping |
| Account Ownwer | Sales Admin by Default |
| Using CE | Checked True |



**Process**  
1. Sales Team members can use this data to proactively identify `Prospect - CE User` accounts that fit their target segement(s). Accounts owned by `Sales Admin` can be adopted by a Sales Team member changing ownership in Salesforce. The adoption of any `Sales Admin` owned records will trigger an email alert that is sent to the Account Research Specialist for transparency and awareness of what account records have been claimed. 
2. The Account Research Specialist will be responsible for reviewing the `Prospect - CE User` accounts on a regular basis to determine additional account records that should be worked either by a Sales Team member or Outbound SDR.
3. When an account record has been identified for follow up, the Account Research Specialist will work with the appropriate Regional Director (RD) to determine Outbound SDR assignment based on work load and available capacity. 
4. The assigned Outbound SDR will work the `Prospect - CE User` account the same as any other known `CE User` account leveraging the tools at their disposal (DiscoverOrg, LinkedIn Sales Navigator, etc) to add contacts to the account record and populate the firmographic profile of the account.   



























[Return to Top of Page](#topofpage)