---
layout: markdown_page
title: "Technical Account Management"
---
# Technical Account Management Handbook
{:.no_toc}

The Technical Account Team is part of [Customer Success](/handbook/customer-success).  The goal of the team is to deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments (Strategic, Large, Mid-Market) so that customers see the value in their GitLab investment, and we retain and drive growth in our customer base.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role
Providing guidance, planning and oversight while leveraging adoption and technical best practices. The Technical Account Manager is the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab. Additionally, the TAM serves as the liaison between the customer and the GitLab ecosystem, streamlining collaboration with Product Management, Engineering, Sales, Professional Services and others.

## Responsibilities
* Contact new customer within x days of purchase
* Own, manage, and deliver consistent onboarding experience based on tier 
* Achieve onboarding milestones
* Send satisfaction survey 90 days post sale
* Actively build and maintain trusted and influential relationships
* Deliver consistent engagement model by tier
* Serve as a main POC
* Increase integrations, identify additional champions, drive utilization
* Help customer realize the value of investment
* Actively seek expansion opportunities
* Identify all impediments

## Customer Segmentation Tier Levels

### Tier 1

*  Customer Segmentation: [Strategic](/handbook/sales/#sts=Market segmentation)
*  $500,000 ARR
*  Accounts Per TAM: 5


### Tier 2
*  Customer Segmentation: [Strategic/Large](/handbook/sales/#sts=Market segmentation)
*  $250,000 ARR
*  Accounts Per TAM: 10



### Tier 3
*  Customer Segmentation: [Mid-Market](/handbook/sales/#sts=Market segmentation)
*  $100,000 ARR
*  Accounts Per TAM: 25

# Engagement Models Defined

![image](images/handbook/sales/tam-table.png)
