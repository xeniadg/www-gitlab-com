---
layout: markdown_page
title: "Customer Success"
---
# Welcome to the Customer Success Handbook
{:.no_toc}

The Customer Success team is part of the [GitLab Sales](/handbook/sales) team who partners with our large and strategic customers to deliver value throughout their journey with GitLab.  

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement
To deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments so that customers see the value in their investment with GitLab, and we retain and drive growth in our within our enterprise customers. 

* The mission of the solutions architect team is to provide these customers with experience in order to:
    * Accelerate initial customer value
    * Maximize long-term, sustainable customer value
    * Improve overall customer satisfaction & referenceability
    * Maximize the total value of the customer to GitLab

    
## Team Initiatives
Our large and strategic customers are in need of an ongoing partnership that combines expert guidance with flexibility and adaptability to support their adoption and continuous improvement initiatives.  

These customers expect that partner to provide a streamlined, consistent and fully coordinated experience that encompasses the full span of their needs as well as the fully lifecycle of the relationship.
Need to focus on 3 main areas in order to grow in our existing accounts as well as land large and strategic:

1. Awareness
2. Adoption
3. Usage


### Initiative: Awareness
Opportunity to improve the overall awareness of GitLab in order to promote and evangelize our brand and solution in a meaningful way to provide big business impact to our customers so that they believe in our vision and strategy.

### Initiative: Adoption
Ensuring paying customers are successful in their onboarding in order to gain adoption and get the most out of our platform and remain happy, paying GitLabers and brand advocates.

### Initiative: Usage
Collecting and making use of customer data and insights is key to customer success.  It’s important to do more with data and when necessary share back with customers, which in turn helps and encourages our customers to improve and drive adoption.


## Responsibilities
* Primarily engaged in a technical consultancy role, providing technical assistance and guidance specific to enterprise level implementations, during the pre-sales process by identifying customers technical and business requirements in order to design a custom GitLab solution.
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab.
* Educate customers of all sizes on the value proposition of Gitlab, and participate in all levels of discussions throughout the organization to ensure our solution is setup for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, GitLab Handbook
* Build deep relationships with senior technical individuals within customers to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales, and Marketing
* Present GitLab platform strategy, concepts, and roadmap to technical leaders with the customer’s organization


## Customer Success Functions

**[Solutions Architects](/handbook/customer-success/engaging-solutions-architect)** are responsible for actively driving and managing the technology evaluation and validation stages of the sales process.

* **Sales Team**: [ When and How to Engage a Solutions Architect](/handbook/customer-success/engaging-solutions-architect)
* **Other GitLabbers**: You can reach out on our Slack channel [#solutions_architects](https://gitlab.slack.com/messages/C5D346V08/) or e-mail [sa-team@gitlab.com](mailto:sa-team@gitlab.com)

**Professional Services**
* [Implementation Specialists](/handbook/customer-success/professional-services) act as the technical representative leading the direct interaction with the customer’s personnel and project teams by rolling out best practices.


**[Technical Account Managers](/handbook/customer-success/tam)** are the key partner helping customers achieve their strategic objectives and maximum value from their investment in GitLab.

## Triage Process
### Engaging with Solutions Architects
For engaging Solutions, Architects, see [this page on the Solutions Architects handbook](/handbook/customer-success/engaging-solutions-architect/).

### The SA Service Desk
The SA team works from the [SA Service Desk](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/boards/339477).  This is the central place for the Sales Team to interact with the SA team and request assistance for customers.  Discovery Calls, POCs, Professional Service Engagements all start as an issue on the SA Service Desk.

To open an issue, visit the [new SA Service Desk issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and select the `SA Activity` template.  This template collects the basic information about the need for SA engagement.  

**Information to Collect**
To help the Solutions Architect team ensure success for your customer, we request the following information *prior* to SA engagement:

* **Customer Information**: Customer name, opportunity size, Sales Force link
* **Preferred Date Options**: When would the customer want to have the call (including time zone information).
* **Opportunity Details**: What is the size of the opportunity?  What is the timeframe for purchase? 
* **Outcome**: What's in it for them?  
* **Challenges**: What roadblocks are they facing?  
* **Infrastructure**: What tools does the customer currently use?
* **SA Tasks**: What do you need from the SA's (demo/technical shotgun/services evaluation/etc.)

**Labels**
* **SA Backlog**: New issues coming into the SA 
* **SA Triaged**: This label will change names to show which SA is triaging this week.  Once the activity has been vetted by the SA team, approved, and SA Ownership is assigned, it is moved to this label
* **SA Doing**: Once an SA signals they are actively working the Issue, the issue is moved to SA Doing.  The issue will stay in this status until the SA and AE agree the Issue has been completed 
* **SA Action Items**: These are long-running strategic issue that the SA group is owning
* **SA Waiting**: This label indicates that the SA team is waiting on the AE for more information before advancing this issue.

* **EXPEDITE**: This label is only to be used if there is <24 hours until the requested event
* **Current Quarter Label**: 
* **Services**: If this opportunity involves or requires services, use this label to indicate that.

### Weekly SA Triage Assignment
Each week one specific Solutions Architect is assigned as the Triage SA for the week.  This is indicated both in the `SA Triaged` label as well as in the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

### Triage Call
The Solutions Architect team hosts a triage call three times a week.

The current week assigned Triage SA facilitates this call.  You can find who the current triager is on the [#solutions_architect](https://gitlab.slack.com/messages/C5D346V08/) channel in Slack.

The call is on Monday, Tuesday, and Thursday at 12:30 pm Easter,n 9:30 am Pacific.  We invited the entire company to join us on these calls, and you can join with [this Zoom link](https://gitlab.zoom.us/j/918526668)

## Other Resources
### Customer Success resource links outside handbook

* [Solutions Architects Onboarding Bootcamp](https://about.gitlab.com/handbook/customer-success/solutions-architects-onboarding-bootcamp/)
* [HealthCheck](https://docs.google.com/document/d/1aHA3W2FsHUApnz2XVtJoyhpcGYy6bgOHoRi4ArXnF0o/edit) Internal Only
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [GitLab University](https://docs.gitlab.com/ce/university/)
* [Our Support Handbook](https://about.gitlab.com/handbook/support/)
* [GitLab Hosted](https://about.gitlab.com/gitlab-hosted/)
* [Workflow SA Demo Scenarios](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit) Internal Only

### Other Sales Topics

* [Sales HandBook](https://about.gitlab.com/handbook/sales/)
* [Sales Standard Operating Procedures](http://about.gitlab.com/handbook/sales/sop/)
* [Sales Operations](https://about.gitlab.com/handbook/sales/salesops/)
* [Sales Skills Best Practices](https://about.gitlab.com/handbook/sales-training/)
* [Sales Discovery Questions](https://about.gitlab.com/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](https://about.gitlab.com/handbook/EE-Product-Qualification-Questions/)
* [GitLab Positioning](https://about.gitlab.com/handbook/positioning-faq/)
* [FAQ from prospects](https://about.gitlab.com/handbook/sales-faq-from-prospects/)
* [CLient Use Cases](https://about.gitlab.com/handbook/use-cases/)
* [POC Template](https://handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24))
* [Sales Demo](/handbook/sales/demo/)
* [Idea to Production Demo](/handbook/product/i2p-demo)
* [Sales Development Team Handbook](/handbook/sales/sdr)
* [Who to go to to ask Questions or Give Feedback on a GitLab feature](https://about.gitlab.com/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](https://about.gitlab.com/handbook/people-operations/ceo-preferences/#sales-meetings)

### Customer On-boarding Process

This content has moved to the [Account Management Handbook](/handbook/account-management/)

### Customer Success & Market Segmentation
For definitions of the account size, refer to [market segmentation](/handbook/sales#market-segmentation) which is based on the _Potential EE Users_ field on the account.

- Strategic: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Large: Sr. Account Executive closed deal and stays on account for growth and renewal.  Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Mid-Market: Account Executive closes deal and transfers to Account Manager to handle renewals and growth. The Account Manager records the Previous Account Owner on the Opportunity Team in any deals while the account is in the New Customer period within the first 12 months including the first annual renewal.
- SMB: Web Portal with Sales Admin oversight. 