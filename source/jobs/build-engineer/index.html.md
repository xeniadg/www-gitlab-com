---
layout: job_page
title: "Build Engineer"
---


## Responsibilities

We are currently hiring self-directed, communicative, experienced build engineers to join GitLab. Our build team closely partners with the rest of the engineering organization to build, configure, and automate GitLab installation. GitLab's build team is tasked with creating a seamless installation experience for customers and community users. [GitLab Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) project enables our users to install, upgrade, and use GitLab easily, and the build team's primary goal is maintaining and improving Omnibus and the infrastructure around it.

Build engineering is interlaced with the broader development team in supporting newly created features and resolving bugs on the omnibus-gitlab project side. Notably, the infrastructure team is the build team's biggest internal customer, so there is significant team interdependency. This team also provides significant variety in tasks and access to a diversity of projects, including helping out on various community packaging projects, etc.  

For a better understanding of how we work, check out the our handbook under [GitLab Workflow](https://about.gitlab.com/handbook/communication/#prioritize).

## Requirements

* Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
* Production level Ruby/RoR
* Extensive Linux experience, comfortable between Debian and RHEL based systems
* Experience with Docker in production use cases
* Basic knowledge of packaging archives such as .deb and .rpm package archives
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* English written and verbal communication skills
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short technical prompt from our Global Recruiters
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first technical interview with our Build Engineering Manager
* Next, candidates will be invited to schedule an interview with a Senior Build Engineer
* Next, candidates will be invited to schedule an interview with the Director of Backend
* Candidates will be invited to schedule a third interview with our VP of Engineering
* Finally, candidates may interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
