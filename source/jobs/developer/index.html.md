---
layout: job_page
title: "Developer"
---

## Developers Roles at GitLab

Developers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and backend or frontend developers to solve common goals.


### Junior Developer

Junior Developers share the same requirements outlined below, but typically join with less or alternate experience in one of the key areas of Developer expertise (Ruby on Rails, Go, Git, reviewing code).


### Intermediate Developer

* Write good code
* Catch bugs and style issues in code reviews
* Ship small features independently
* Be positive and solution oriented
* Good communication: Regularly achieve consensus with peers, and clear status updates
* Constantly improve product quality, security, and performance


### Senior Developer

The Senior Developer role extends the [Developer](#developer) role.

* Write _great_ code
* Teach and enforce architectural patterns in code reviews
* Ship _medium_ features independently
* Generate architecture recommendations
* _Great_ communication: Regularly achieve consensus amongst _teams_
* Perform technical interviews

***

A Senior Developer may want to pursue the [engineering management track](/jobs/engineering-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***


### Staff Developer

The Staff Developer role extends the [Senior Developer](#senior-developer) role.

* Write _exquisite_ code
* Ship _large_ features independently
* Make architecture decisions
* Implement technical and process improvements
* _Exquisite_ communication: Regularly achieve consensus amongst _departments_
* Author technical architecture documents for epics
* Train others to perform technical interviews and author code tests (where applicable)
* Write public blog posts


### Distinguished Developer

The Distinguished Developer role extends the [Staff Developer](#staff-developer) role.

* Ship _large_ feature sets with team
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for developers
* Author architecture documents for epics
* Hold team members accountable within their roles


### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Developer](#distinguished-developer) role.

* Work on our hardest technical problems
* Ship _extra-large_ feature sets with team
* Help create the sense of psychological safety in the department
* Represent the company publicly at conferences
* Work directly with customers

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule a behavioral interview with either our Discussion Lead or Platform Engineering Manager
- Next, candidates will be invited to schedule a technical interview with either our Discussion Lead or Platform Engineering Manager
- Candidates will then be invited to schedule an interview with Director of Backend
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
