---
layout: job_page
title: "Engineering Manager - Geo"
---

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html) is an
enterprise product feature set that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well. As the Engineering Manager for the Geo team, you will take overall responsibility for
hiring, team management, agile project management, and the quality of the
feature set. This position will report to the Director of Backend.

## Responsibilities

As an Engineering Manager you are expected to
- Be a steward of product quality
   - Author project plans for epics
   - Run agile project management processes
   - Conduct code reviews, and make technical contributions to product
   architecture as well as getting involved in solving bugs and delivering small
   features
- Be a leader for the team
   - Draft quarterly OKRs and have regular 1:1’s with team
   - Actively seek and hire globally-distributed talent
   - Conduct managerial interviews for applicants, and train the team to screen applicants
   - Contribute to the sense of psychological safety on your team
   - Generate and implement process improvements

For the Geo team specifically, you will be involved in - and responsible for -
- Architecting Geo and Disaster Recovery products for GitLab
- Identifying ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
- Instrument and monitor the health of distributed GitLab instances
- Educate all team members on best practices relating to high availability

## Requirements

- 5 years or more experience in a leadership role with current technical experience
- Experience architecting and implementing fault-tolerant, distributed systems
- In-depth experience with Ruby on Rails, Go, and/or Git
- Excellent written and verbal communication skills
- You share our [values](/handbook/values), and work in accordance with those values
- [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

## Hiring process

The hiring process for this position includes

- A screening call with one of our Recruiters
- An interview with the interim Engineering Manager - Geo
- An interview with the Director of Backend
- An interview with a Senior Developer with deep knowledge of the Geo feature set
- An interview with the VP of Engineering

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
