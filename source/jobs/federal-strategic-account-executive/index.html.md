---
layout: job_page
title: "Federal Strategic Account Executive"
---

GitLab is seeking an experienced, high-energy federal sales professional with a proven track record of over achieving quota. As the individual who represents GitLab and drives the revenue, this person must be a closer who can create a large pipeline of business within a short period of time and work with existing clients as well as new logo opportunities. This person should be located in the Washington, DC area.

## Responsibilities

- Conduct sales activities including prospecting and developing opportunities in large/strategic accounts
- Ensure the successful rollout and adoption of Gitlab products through strong account management activities and coordination with pre-and-post sales engineering and support resources
- Travel as necessary to accounts in order to develop relationships and close large opportunities
- Generate qualified leads and develop new customers in conjunction with our strategic channel partners in exceeding quota.
- Expand knowledge of industry as well as the competitive posture of the company
- Prepare activity and forecast reports as requested
- Update and maintain Sales’ database as appropriate
- Assist sales management in conveying customer needs to product managers, and technical support staff
- Utilize a consultative approach, discuss business issues with prospect and develop a formal quote, a written sales proposal or a formal sales presentation addressing their business needs.
- Respond to RFP's and follow up with prospects.
- Develop an account plan to sell to customers based on their business needs.
- Build and strengthen the business relationship with current accounts and new prospects.
- Recommend marketing strategies.

## Requirements

- Able to provide high degree of major account management and control
- Work under minimal supervision on complex projects.
- Strong interpersonal skills and ability to excel in a team oriented atmosphere
- Strong written/verbal communications skills
- 5+ years successful quota attainment in the Federal Space
- Very motivated and goal-oriented
- Must want a career-oriented environment that is both fun and professional.
- Strong customer service orientation and ability to develop and maintain relationships
- Preferred experience with Git, Software Development Tools, Application Lifecycle Management
- You share our values, and work in accordance with those values.


## Federal Strategic Account Executive - DOD

The Federal Strategic Account Executive - DOD has all of the responsibilities and requirements as the Federal Strategic Account Executive, with the additional requirement of:

- 5+ years of experience selling into Federal DOD accounts with a comprehensive understanding of the local military landscape and associated business processes.

## Federal Strategic Account Executive - Intel

The Federal Strategic Account Executive - Intel has all of the responsibilities and requirements as the Federal Strategic Account Executive, with the additional requirement of:

- Must have security clearance
- 5+ years of experience selling into Federal Intel accounts with a comprehensive understanding of the intelligence community landscape and associated business processes.

## Federal Strategic Account Executive - Civilian

The Federal Strategic Account Executive - Civilian has all of the responsibilities and requirements as the Federal Strategic Account Executive, with the additional requirement of:

- 5+ years of experience selling into Federal Civilian accounts with a comprehensive understanding of the local public sector landscape and associated business processes.

## Hiring Process
Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

- Candidates will be given a questionnaire to complete
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with our Global Recruiters
- Next, candidates will be invited to schedule a first interview with our Federal Account Executive
- Candidates will then be invited to schedule an interview with the Director of Federal Sales
- Candidates will be invited to schedule a third interview with our Chief Revenue Officer
- Finally, candidates may interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).


## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
