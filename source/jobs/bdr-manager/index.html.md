---
layout: job_page
title: "Sales and Business Development Manager"
---

{: .text-center}
<br>

You have experience in sales and business development helping people get started with a technical product. Your job is four-fold: (1) Help generate and increase interest in GitLab, (2) connect people interested in GitLab to the sales team or other appropriate resources, (3) lead and train other members of the business development team, and (4) take on operational and administrative tasks to help the sales and business development team perform.

## Responsibilities

* Meet or exceed Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Lead the Sales Development team by:
    * Partnering with Regional Sales Directors (RDs) to identify key company accounts for the Sales Development Representitives (SDRs) to focus on.
    * Ensuring key accounts benefit from coordinated activity from SDRs, Digital Marketing, and Field Marketing.
    * Training other members of the Sales Development team to navigate key accounts to uncover potential sales opportunity.
* Meet or exceed volume and value targets for Business Development Representative (BDR) qualified leads that convert to Sales Qualified Leads (SQL).
* Lead the Business Development team by:
    * Identifying where leads are in their buying process and taking appropriate action to help them on their way to becoming a GitLab customer.
    * Ensuring BDR qualified leads are assigned to the sales team and are followed up with effectively.
    * Training other members of the business development team to identify, qualify, and assign inbound leads.
* Lead hiring and onboarding of new sales and business development representatives.
* Ensuring all team members improve performance and abilities over time by providing coaching and feedback during weekly 1:1s and team calls.
* Work with Digital Marketing on coordinating inbound and outbound marketing tactics with SDR and BDR activities.
* Work with Content Marketing on refining the message and content offers we take to market.
* Work with Field Marketing on event lead qualification, event registration/attendance, and follow up.
* Work with the Sales team to ensure relationships with key accounts are developed and strengthened.
* Identify and define key sales metrics, measure lead-to-opportunity process, and create goals that drive growth.
* Document all processes in the handbook and update as needed.
* Providing input to Chief Marketing Officer and Senior Director of Marketing & Sales Development regarding sales and business development representative team practices.

## Requirements

* Excellent spoken and written English
* Experience in sales, marketing, or customer service for a technical product - leadership experience is highly preferred.
* Experience with CRM software (Salesforce preferred)
* Experience in sales operations and/or marketing automation software preferred
* Understanding of B2B software, Open Source software, and the developer product space is preferred
* Passionate about technology and learning more about GitLab
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).


