---
layout: job_page
title: "Demand Generation Specialist"
---

## Responsibilities

* Own the marketing funnel for our GitLab on-premises offerings (GitLab CE and GitLab EE). 
From top of the funnel to our already existing customers, your role is to increase conversion and volume.
* Engage with the Director of Demand Generation to build the marketing team in accordance with the values of GitLab. 
* Implement creative and effective ideas for bringing new leads into the funnel through online campaigns, channel marketing, 
technical webcasts, non-spammy email campaigns, etc.
* Create a better experience online so that it is easy for our enterprise customers to upgrade from GitLab CE to EE easily.
* Work with our super smart sales team to create the proper follow-up materials for our customers interested in GitLab Enterprise Edition.
* Collaborate with sales to develop channel marketing and partner marketing strategy for increasing demand through partnerships.
* Familiar with or ready to learn Marketo, Hubspot, and/or Pardot to help the team manage leads, email campaigns, and analytics. 
Not afraid to dig around in data to help create reports for the team meetings. Comfortable being a self-taught marketing automation master.
* Work with sales operations to make sure the leads flowing from our marketing automation software to Salesforce are correct and timely.
* Work with sales to make sure the proper reports are generated in order to track leads.

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).

