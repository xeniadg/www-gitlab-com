### Release post - GitLab X.Y :rocket:

**Review Apps**: https://release-x-y.about.gitlab.com/YYYY/MM/22/gitlab-x-y-released/

_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/source/posts/YYYY-MM-22-gitlab-x-y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/release_posts/YYYY_MM_22_gitlab_x_y_released.yml
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-x-y/source/images/x_y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/features.yml
- **Promo banner**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/promo.yml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-x-y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/

### General Contributions

**Due date: MM/DD** (6th working day before the 22nd)

All contributions added by team members, collaborators, and Product Managers (PMs).

#### Author's checklist

**Due date: MM/DD** (6th working day before the 22nd)

The PM leading the post is responsible for adding and checking the following items:

- [ ] Label MR: ~"blog post" ~release
- [ ] Assign the MR to yourself
- [ ] Add milestone
- [ ] Update the links and due dates in this MR description
- [ ] Make sure the blog post initial files, as well as this MR template contain the latest templates
- [ ] Add authorship (author's data)
- [ ] Add Introduction
- [ ] Add MVP (feature block)
- [ ] Add MVP to `data/mvps.yml`
- [ ] Add cover image `image_title` (compressed)
- [ ] Add Performance improvements
- [ ] Add Omnibus improvements
- [ ] Add GitLab Runner changes
- [ ] Add Upgrade barometer (check with the release manager)
- [ ] Check which one is the top feature (with Job and William)
- [ ] Check if deprecations are included
- [ ] Alert people one working day before each due date (post a comment to #release-post Slack channel)
- [ ] Perform the [content review](#content-review)
- [ ] Assign the MR to the next reviewer

#### Feature blocks

**Due date: MM/DD** (6th working day before the 22nd)

The Product Managers are responsible for adding their feature blocks to the
release post by the due date for general contributions.

PMs: please check your box only when **all** your features and deprecations were
added with completion (documentation links, images, etc). Pls don't check if
there are still things missing.

- [ ] Fabio
- [ ] James
- [ ] Joshua
- [ ] Mark
- [ ] Mike
- [ ] Victor

Tip: make your own checklist:

- Primary features
- Improvements (secondary features)
- Deprecations
- Documentation updated
- Documentation links added to the post
- Illustrations added to the post (compressed)
- Update `features.yml` (with accompanying screenshots)

### Review

Ideally, complete the review until the **4th** working day before the 22nd,
so that the 3rd and the 2nd working day before the release
could be left for fixes and small improvements.

#### Content review

**Due date: MM/DD** (2nd working day before the 22nd)

Performed by the PM leading the post:

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] Check all comments in the thread (make sure no contribution was left behind)
- [ ] Check Features' names
- [ ] Check Features' availability (CE, EES, EEP, EEU badges)
- [ ] Check Documentation links (all feature blocks contain `documentation_link`)
- [ ] Make sure `documentation_link` links to feature webpages when available
- [ ] Update `data/promo.yml`
- [ ] Features were added to `data/features.yml` (with accompanying screenshots)
- [ ] Check all images size < 300KB (compress them all with [TinyPNG](https://tinypng.com/) or similar tool)
- [ ] Pull `master`, resolve any conflicts
- [ ] Make sure all discussions in the thread are resolved

#### Further reviews

**Due date: MM/DD** (2nd working day before the 22nd)

- [ ] Copyedit (Rebecca, Axil, or Marcia)
  - [ ] Title
  - [ ] Description
  - [ ] Grammar, spelling, clearness (body)
  - [ ] [Homepage Blurb](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/home/ten-oh-announcement.html.haml)
  - [ ] Tweet Text
- [ ] Marketing review (William)
  - [ ] Social sharing text (for Twitter, FB, and LinkedIn)
- [ ] Final review (Job)

#### Structural Check

**Due date: MM/DD** (2nd working day before the 22nd)

Performed by technical writers (Axil or Marcia):

- 1. Structural check
- [ ] Label MR: ~"blog post" ~release ~review-structure
- [ ] Check frontmatter (entries, syntax)
- [ ] Check `image_title` and `twitter_image`
- [ ] Check image shadow `{:.shadow}`
- [ ] Check images' `ALT` text
- [ ] Videos/iframes wrapped in `<figure>` tags (responsiveness)
- [ ] Add/check `<!-- more -->` separator
- [ ] Add/check cover img reference (at the end of the post)
- [ ] Columns (content balance between the columns)
- [ ] Meaningful links (SEO)
- [ ] Badges consistency (applied to all headings)
- [ ] Double check documentation updates
- [ ] Check documentation links (point to `/ee/`, not to `/ce/`)
- [ ] Remove any remaining instructions
- [ ] Remove HTML comments
- [ ] Run deadlink checker (https://www.deadlinkchecker.com/)
- [ ] Pull `master`
- [ ] Update release template with any changes (if necessary)

### Merge it :rocket:

The PM leading the post is responsible for merging the MR and following up
with possible adjustments/fixes.

The post is to be merged on the **22nd** as soon as
GitLab.com is up and running on the new release
version, and all packages are publicly available. Not before.
The usual release time is **15:00 UTC** but it varies according to
the deployment.
Coordinate the timing with the
[release managers](https://about.gitlab.com/release-managers/).
Ask them to keep you in the loop.

After merging, wait a few minutes to see if no one spot an error (usually posted in #general),
then share on Twitter, Facebook, and LinkedIn, or make sure someone does.
